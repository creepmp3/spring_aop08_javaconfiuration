package kr.co.hbilab.app;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.util.StopWatch;

/**
 * 2015. 5. 28.
 * @author user
 * 
 */
public class CheckTime implements MethodInterceptor{

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        StopWatch sw = new StopWatch();
        sw.start();
        Object obj = invocation.proceed();
        sw.stop();
        System.out.println("처리시간 : " + sw.getTotalTimeSeconds());
        return null;
    }
    
}
